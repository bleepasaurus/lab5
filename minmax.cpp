#include <iostream>
#include <time.h>
#include <stdlib.h>

using namespace std;

int main() {
  srand(time(0)); 
  int randomNumbers[10], mini=100, maxi=0;

  // rand init
  for (int i=0; i<10; i++){
    randomNumbers[i] = rand() % 100;
  }

  // display random
  cout << "Random numbers: ";
  for (int i=0; i<10; i++){
    cout << randomNumbers[i] << " ";
  }
  cout << endl;
  
  // minimum
  for (int i: randomNumbers){
    if (mini > i)
      mini = i;
    if (maxi < i)
      maxi = i;
  }
  cout << "minimum is: " << mini << " maximum is: " << maxi;
}
