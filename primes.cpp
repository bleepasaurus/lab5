#include <iostream>
using namespace std;

int isPrimeNumber(int);

int main()
{
  int start;
  cout << "Enter an integer: ";
  cin >> start;
  cout << "Primes to " << start << " are: ";

  bool isPrime;
   for(int n = 2; n <= start; n++) {
      isPrime = true;

      for(int i = 2; i <= n/2; i++) {
        if (n%i == 0) {
          isPrime = false;
          break;
        }
      }  

      if(isPrime == true)
         cout<<n<<" ";
   }
   return 0;
}
